import Vue from "vue";
import ToggleButton from "vue-js-toggle-button";
import VueRouter from "vue-router";
import routes from "./routes";
import vueXlsxTable from "vue-xlsx-table";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueRouter);
Vue.use(ToggleButton);
Vue.use(vueXlsxTable, {
  rABS: false
});

Vue.use(VueAxios, axios);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDfkvwshPDlNA_U0CmYHQZjDQPABTBndG4",
  authDomain: "winneradventure-ed4a1.firebaseapp.com",
  databaseURL: "https://winneradventure-ed4a1.firebaseio.com",
  projectId: "winneradventure-ed4a1",
  storageBucket: "winneradventure-ed4a1.appspot.com",
  messagingSenderId: "422902637715",
  appId: "1:422902637715:web:438cffdb3cb09f4ac91966"
};

// var firebaseConfig = {
//   apiKey: "AIzaSyAO6XZYR9cTvXCI-0orTpwlYm3Q2ys-FlY",
//   authDomain: "winnerenglish-5f8d3.firebaseapp.com",
//   databaseURL: "https://winnerenglish-5f8d3.firebaseio.com",
//   projectId: "winnerenglish-5f8d3",
//   storageBucket: "winnerenglish-5f8d3.appspot.com",
//   messagingSenderId: "40516029824",
//   appId: "1:40516029824:web:f5e3d3a9583427c9"
// };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();
const storage = firebase.storage();
export const st = storage.ref();

Vue.mixin({
  data() {
    return {
      version: "1.0.8", // update new 1/4/2020
      apiPath: "https://api.winner-english.com/data/",
      setRoom: [
        {
          value: "p1",
          label: "ป.1"
        },
        {
          value: "p2",
          label: "ป.2"
        },
        {
          value: "p3",
          label: "ป.3"
        },
        {
          value: "p4",
          label: "ป.4"
        },
        {
          value: "p5",
          label: "ป.5"
        },
        {
          value: "p6",
          label: "ป.6"
        },
        {
          value: "m1",
          label: "ม.1"
        },
        {
          value: "m2",
          label: "ม.2"
        },
        {
          value: "m3",
          label: "ม.3"
        },
        {
          value: "m4",
          label: "ม.4"
        },
        {
          value: "m5",
          label: "ม.5"
        },
        {
          value: "m6",
          label: "ม.6"
        }
      ]
    };
  },
  methods: {
    playsound(url) {
      let audio = new Audio(url);
      audio.play();
    },
    // ฟังก์ชันการสลับอาเรย์ สุ่มอาเรย์
    shuffle(array) {
      array.sort(() => Math.random() - 0.5);
    },
    notifyRed(messages) {
      this.$q.notify({
        color: "negative",
        position: "top",
        icon: "error",
        message: messages,
        timeout: 800
      });
    },
    notifyGreen(messages) {
      this.$q.notify({
        color: "secondary",
        position: "top",
        icon: "done",
        message: messages,
        timeout: 800
      });
    },
    loadingShow() {
      this.$q.loading.show({
        delay: 400,
        spinner: "QSpinnerHourglass"
      });
    },
    loadingHide() {
      this.$q.loading.hide();
    },

    loadUnit(levelKey) {
      //ส่งคืน array ใน localstorage 'unitlist' เรียงจำนวน unit โดยการส่ง level key ปัจจุบัน
      let unitList = [];
      let levelData = this.$q.localStorage.getItem("levelData");
      let levelData2 = levelData.filter(x => {
        return x.key == levelKey;
      });

      for (let index = 1; index <= levelData2[0].unit; index++) {
        unitList.push(index.toString());
      }
      this.$q.localStorage.set("unitList", unitList);
    },

    schoolNameShow(schoolKey) {
      let schoolData = this.$q.localStorage.getItem("schoolData");
      let schoolData2 = schoolData.filter(x => {
        return x.key == schoolKey;
      });

      return (
        schoolData2[0].schoolCode + " - " + "โรงเรียน" + schoolData2[0].name
      );
    },
    async getAcademicYear() {
      let getTime = await axios.get(
        "https://api.winner-english.com/data/api/gettime.php"
      );
      getTime = getTime.data[0]["date"];
      getTime = getTime.split("/");
      let currentYear = getTime[2];
      currentYear = Number(currentYear) + 543;

      let currentMonth = getTime[1];
      currentYear =
        currentMonth >= 1 && currentMonth <= 4 ? currentYear - 1 : currentYear;

      return Number(currentYear);
    }
  }
});
export default function(/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      x: 0,
      y: 0
    }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
