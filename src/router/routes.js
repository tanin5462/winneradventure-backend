const routes = [
  //POC Score evaluation
  {
    path: "/scoretest",
    component: () => import("pages/monitor/scoretest.vue"),
    name: "scoretest"
  },
  {
    path: "",
    component: () => import("pages/login.vue")
  },
  {
    path: "/welcomeback",
    component: () => import("pages/welcomeback.vue"),
    name: "welcomeback"
  },
  //  ออกจากระบบ
  {
    path: "/logout",
    component: () => import("pages/logout.vue"),
    name: "logout"
  },
  {
    path: "/test",
    component: () => import("pages/Index.vue"),
    name: "test"
  },
  {
    path: "/test2",
    component: () => import("pages/test2.vue"),
    name: "test2"
  },
  {
    path: "/monitor/dev",
    component: () => import("pages/monitorDev.vue"),
    name: "monitordev"
  },
  {
    path: "/import",
    component: () => import("pages/import/import.vue"),
    name: "import"
  },
  {
    path: "/level/content/:level",
    component: () => import("pages/level/levelcontent.vue"),
    name: "levelcontent"
  },
  {
    path: "/schoolprint",
    component: () => import("pages/school/print.vue"),
    name: "schoolprint"
  },
  {
    path: "/exam/print",
    component: () => import("pages/practice/examprint.vue"),
    name: "examprint"
  },
  {
    path: "/placement/print",
    component: () => import("pages/practice/placementprint.vue"),
    name: "placementprint"
  },
  {
    path: "/studentprint",
    component: () => import("pages/user/studentprint.vue"),
    name: "studentprint"
  },
  {
    path: "/flashcardprint",
    component: () => import("pages/practice/flashcardprint.vue"),
    name: "flashcardprint"
  },
  {
    path: "/vdogrammarprint",
    component: () => import("pages/practice/vdogrammarprint.vue"),
    name: "vdogrammarprint"
  },
  {
    path: "/practice/translation/print",
    component: () => import("pages/practice/translationprint.vue"),
    name: "translationprint"
  },
  {
    path: "/practice/multiplechoice/print",
    component: () => import("pages/practice/multiplechoiceprint.vue"),
    name: "multiplechoiceprint"
  },
  {
    path: "/questionnaireprint",
    component: () => import("pages/practice/questionnaireprint.vue"),
    name: "questionnaireprint"
  },
  {
    path: "/practice/fillinprint",
    component: () => import("pages/practice/fillinprint.vue"),
    name: "fillinprint"
  },
  {
    path: "/practice/phonicsprint/:key",
    component: () => import("pages/practice/phonicsprint.vue"),
    name: "phonicsprint"
  },
  {
    path: "/readingprint",
    component: () => import("pages/practice/readingprint.vue"),
    name: "readingprint"
  },
  {
    path: "/practice/conversationprint",
    component: () => import("pages/practice/conversationprint.vue"),
    name: "conversationprint"
  },
  {
    path: "/practice/languagetipsprint",
    component: () => import("pages/practice/languagetipsprint.vue"),
    name: "languagetipsprint"
  },
  {
    path: "/monitor/questionnaire/print",
    component: () => import("pages/monitor/monitorquestionnaireprint.vue"),
    name: "monitorquestionnaireprint"
  },
  {
    path: "/monitor/pretest/graph",
    component: () => import("pages/monitor/monitorpretestgraph.vue"),
    name: "monitorpretestgraph"
  },
  {
    path: "/monitor/pretest/table",
    component: () => import("pages/monitor/monitorpretesttable.vue"),
    name: "monitorpretesttable"
  },

  {
    path: "/monitor/posttest/graph",
    component: () => import("pages/monitor/monitorposttestgraph.vue"),
    name: "monitorposttestgraph"
  },
  {
    path: "/monitor/posttest/table",
    component: () => import("pages/monitor/monitorposttesttable.vue"),
    name: "monitorposttesttable"
  },
  {
    path: "/monitor/posttest/tablediff",
    component: () => import("pages/monitor/monitorposttesttablediff.vue"),
    name: "monitorposttesttablediff"
  },
  {
    path: "/monitor/posttest/graphdiff",
    component: () => import("pages/monitor/monitorposttestgraphdiff.vue"),
    name: "monitorposttesgraphdiff"
  },
  {
    path: "/monitor/placement/table",
    component: () => import("pages/monitor/monitorplacementtable.vue"),
    name: "monitorplacementtable"
  },
  {
    path: "/monitor/placement/graph",
    component: () => import("pages/monitor/monitorplacementgraph.vue"),
    name: "monitorplacementgraph"
  },

  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [


      // ระบบติดตาม
      {
        path: "/monitor",
        component: () => import("pages/monitor/monitorwelcome.vue"),
        name: "monitor"
      },
      {
        path: "/monitor/placement",
        component: () => import("pages/monitor/monitorplacement.vue"),
        name: "monitorplacement"
      },
      {
        path: "/monitor/pretest",
        component: () => import("pages/monitor/monitorpretest.vue"),
        name: "monitorpretest"
      },
      {
        path: "/monitor/study",
        component: () => import("pages/monitor/monitorstudy.vue"),
        name: "monitorstudy"
      },
      {
        path: "/monitor/studyoverview",
        component: () => import("pages/monitor/monitorstudyoverview.vue"),
        name: "monitorstudyoverview"
      },
      {
        path: "/monitor/studyexercise",
        component: () => import("pages/monitor/monitorstudyexercise.vue"),
        name: "monitorstudyexercise"
      },
      {
        path: "/monitor/posttest",
        component: () => import("pages/monitor/monitorposttest.vue"),
        name: "monitorposttest"
      },
      {
        path: "/monitor/questionnaire",
        component: () => import("pages/monitor/monitorquestionnaire.vue"),
        name: "monitorquestionnaire"
      },

      {
        path: "/monitor/print",
        component: () => import("pages/monitor/monitorprint.vue"),
        name: "monitorprint"
      },
      // ระดับการเรียน
      {
        path: "/level",
        component: () => import("pages/level/levelmain.vue"),
        name: "level"
      },

      // แบบฝึกหัด
      {
        path: "/practice",
        component: () => import("pages/practice/practicemain.vue"),
        name: "practice"
      },
      {
        path: "/practiceSyncCheck",
        component: () => import("pages/practice/practiceSyncCheck.vue"),
        name: "practiceSyncCheck"
      },
      // VDO grammar
      {
        path: "/practice/vdogrammar/:key",
        component: () => import("pages/practice/vdogrammar.vue"),
        name: "vdogrammar"
      },
      {
        path: "/practice/vdogrammaradd/:key",
        component: () => import("pages/practice/vdogrammaradd.vue"),
        name: "vdogrammaradd"
      },
      {
        path: "/practice/vdogrammaredit/:key/:vdokey",
        component: () => import("pages/practice/vdogrammaradd.vue"),
        name: "vdogrammaredit"
      },
      // flashcard
      {
        path: "/practice/flashcard/:key",
        component: () => import("pages/practice/flashcardmain.vue"),
        name: "flashcard"
      },
      {
        path: "/practice/flashcardadd/:key",
        component: () => import("pages/practice/flashcardadd.vue"),
        name: "flashcardadd"
      },
      {
        path: "/practice/flashcardedit/:key/:vocabkey",
        component: () => import("pages/practice/flashcardadd.vue"),
        name: "flashcardedit"
      },
      // translation
      {
        path: "/practice/translation/:key",
        component: () => import("pages/practice/translationmain.vue"),
        name: "translation"
      },
      {
        path: "/practice/translationadd/:key",
        component: () => import("pages/practice/translationaddedit.vue"),
        name: "translationadd"
      },
      {
        path: "/practice/translationedit/:key/:translationkey",
        component: () => import("pages/practice/translationaddedit.vue"),
        name: "translationedit"
      },

      {
        path: "/practice/translationvdo/:key",
        component: () => import("pages/practice/translationvdo.vue"),
        name: "translationvdo"
      },
      {
        path: "/practice/translationvdoadd/:key",
        component: () => import("pages/practice/translationvdoaddedit.vue"),
        name: "translationvdoadd"
      },
      {
        path: "/practice/translationvdoedit/:key/:vdokey",
        component: () => import("pages/practice/translationvdoaddedit.vue"),
        name: "translationvdoedit"
      },
      //  multiplechoice
      // 0 = questionsound
      // 1 = answersound
      // 2 = ปกติ
      {
        path: "/practice/multiplechoice/:key/:type",
        component: () => import("pages/practice/multiplechoicemain.vue"),
        name: "multiplechoicemain"
      },
      {
        path: "/practice/multiplechoiceadd/:key/:type",
        component: () => import("pages/practice/multiplechoiceaddedit.vue"),
        name: "multiplechoiceadd"
      },
      {
        path: "/practice/multiplechoiceedit/:practicekey/:key/:type/:page?",
        component: () => import("pages/practice/multiplechoiceaddedit.vue"),
        name: "multiplechoiceedit"
      },
      // grammar fill in
      {
        path: "/practice/fillin/:key",
        component: () => import("pages/practice/fillinmain.vue"),
        name: "fillinmain"
      },
      {
        path: "/practice/fillinadd/:key",
        component: () => import("pages/practice/fillinadd.vue"),
        name: "fillinadd"
      },

      {
        path: "/practice/fillinedit/:practicekey/:key/:page?",
        component: () => import("pages/practice/fillinadd.vue"),
        name: "fillinedit"
      },

      // phonics
      {
        path: "/practice/phonics/:key",
        component: () => import("pages/practice/phonicsmain.vue"),
        name: "phonicsmain"
      },
      {
        path: "/practice/phonicsadd/:key",
        component: () => import("pages/practice/phonicsaddedit.vue"),
        name: "phonicsadd"
      },
      {
        path: "/practice/phonicsedit/:practicekey/:key",
        component: () => import("pages/practice/phonicsaddedit.vue"),
        name: "phonicsedit"
      },

      // reading type 1 = fillin , 2 = multiple choices
      {
        path: "/practice/reading/:key/:type/:page",
        component: () => import("pages/practice/readingmain.vue"),
        name: "readingmain"
      },
      {
        path: "/practice/reading/add/:key/:level/:unit/:type",
        component: () => import("pages/practice/readingadd.vue"),
        name: "readingadd"
      },
      {
        path:
          "/practice/reading/edit/:key/:practicekey/:level/:unit/:page/:type",
        component: () => import("pages/practice/readingadd.vue"),
        name: "readingedit"
      },
      {
        path: "/landing/:key1/:key2/:key3/:key4/:key5/:key6",
        component: () => import("pages/practice/landing.vue"),
        name: "landing"
      },
      {
        path: "/landingmulti/:key1/:key2/:key3",
        component: () => import("pages/practice/landingmulti.vue"),
        name: "landingmulti"
      },
      {
        path: "/landingfillin/:key1/:key2",
        component: () => import("pages/practice/landingfillin.vue"),
        name: "landingfillin"
      },

      //Languagetips
      {
        path: "/practice/languagetips/:key",
        component: () => import("pages/practice/languagetips.vue"),
        name: "languagetips"
      },
      {
        path: "/practice/languagetips/add/:key",
        component: () => import("pages/practice/languagetipsadd.vue"),
        name: "languagetipsadd"
      },
      {
        path: "/practice/languagetips/edit/:key/:practicekey",
        component: () => import("pages/practice/languagetipsadd.vue"),
        name: "languagetipsedit"
      },

      // conversation

      {
        path: "/practice/conversation/:key",
        component: () => import("pages/practice/conversationmain.vue"),
        name: "conversationmain"
      },

      // เสียง
      {
        path: "/sound",
        component: () => import("pages/practice/soundmain.vue"),
        name: "sound"
      },
      // คลังข้อสอบ
      {
        path: "/exam",
        component: () => import("pages/practice/exammain.vue"),
        name: "exam"
      },
      {
        path: "/exam/add",
        component: () => import("pages/practice/examaddedit.vue"),
        name: "examadd"
      },
      {
        path: "/exam/edit/:key",
        component: () => import("pages/practice/examaddedit.vue"),
        name: "examedit"
      },
      {
        path: "/exam/sync",
        component: () => import("pages/practice/examsync.vue"),
        name: "examsync"
      },

      // วัดระดับ
      {
        path: "/placement",
        component: () => import("pages/practice/placementmain.vue"),
        name: "placement"
      },

      {
        path: "/placement/add",
        component: () => import("pages/practice/placementaddedit.vue"),
        name: "placementadd"
      },
      {
        path: "/placement/edit/:key",
        component: () => import("pages/practice/placementaddedit.vue"),
        name: "placementedit"
      },
      // ความพึ่งพอใจ
      {
        path: "/questionnaire",
        component: () => import("pages/practice/questionnairemain.vue"),
        name: "questionnaire"
      },
      // โรงเรียน
      {
        path: "/school",
        component: () => import("pages/school/schoolmain.vue"),
        name: "school"
      },
      {
        path: "/school/add",
        component: () => import("pages/school/schooladd.vue"),
        name: "schooladd"
      },
      {
        path: "/school/edit/:key",
        component: () => import("pages/school/schooladd.vue"),
        name: "schooledit"
      },

      // นักเรียน
      {
        path: "/student",
        component: () => import("pages/user/studentmain.vue"),
        name: "student"
      },
      {
        path: "/student/add/",
        component: () => import("pages/user/studentadd.vue"),
        name: "studentadd"
      },
      {
        path: "/student/edit/:key",
        component: () => import("pages/user/studentadd.vue"),
        name: "studentedit"
      },
      {
        path: "/student/addcheck",
        component: () => import("pages/user/studentaddcheck.vue"),
        name: "studentaddcheck"
      },
      // คุณครู
      {
        path: "/teacher",
        component: () => import("pages/user/teachermain.vue"),
        name: "teacher"
      },
      {
        path: "/teacher/add/:schoolKey",
        component: () => import("pages/user/teacheraddedit.vue"),
        name: "teacheradd"
      },
      {
        path: "/teacher/edit/:teacherKey/:schoolKey",
        component: () => import("pages/user/teacheraddedit.vue"),
        name: "teacheredit"
      },

      // ผู้ดูแลระบบ
      {
        path: "/user",
        component: () => import("pages/user/usermain.vue"),
        name: "user"
      },
      {
        path: "/user/add",
        component: () => import("pages/user/useraddedit.vue"),
        name: "useradd"
      },
      {
        path: "/user/edit/:key",
        component: () => import("pages/user/useraddedit.vue"),
        name: "useredit"
      },
      {
        path: "/user/profile/",
        component: () => import("pages/user/userprofile.vue"),
        name: "userprofile"
      },
      {
        path: "/gendata/",
        component: () => import("pages/gendata/gendata.vue"),
        name: "gendata"
      }
    ]
  }
];

// Always leave this as last one@click="usermain()"
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
